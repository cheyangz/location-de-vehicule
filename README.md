# location de vehicule

Une agence de location de véhicules souhaite se doter d'un logiciel lui permettant de gérer son système d'information. Ce système doit gérer la location (réservation en ligne, par téléphone, à l'agence), le parc automobile, et l'entretien de véhicule